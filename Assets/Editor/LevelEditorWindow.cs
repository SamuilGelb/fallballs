﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Linq;
using System.Collections.Generic;

public class LevelEditorWindow : EditorWindow
{
    [MenuItem("Assets/UpdateLevelList")]
    private static void UpdateLevelList()
    {
        var path = "";
        var obj = Selection.activeObject;
        if (obj == null) path = "Assets";
        else path = AssetDatabase.GetAssetPath(obj.GetInstanceID());
        List<string> levelNames = Directory.GetFiles(path, "*.txt").ToList();
        List<int> levels = new List<int>();
        int c = levelNames.Count;
        for (int i = 0; i < c; i++)
        {
            levelNames[i] = levelNames[i].Replace("Assets/Resources/Levels\\level_", "").Replace(".txt", "");
            levels.Add(Convert.ToInt32(levelNames[i]));
        }
        levels.Sort();
        c = levels.Count;
        string dataToWrite = $"{{\"Count\": {c}, \"Levels\":[";
        for (int i = 0; i < c; i++)
        {
            dataToWrite = $"{dataToWrite}{levels[i]}{(i + 1 >= c ? "" : ", ")}";
        }
        dataToWrite = $"{dataToWrite}]}}";
        File.WriteAllText(Path.Combine(path, "LevelList.json"), dataToWrite);
    }

    [MenuItem("Assets/UpdateLevelList", true)]
    private static bool UpdateLevelListValidate()
    {
        var path = "";
        var obj = Selection.activeObject;
        if (obj == null) path = "Assets";
        else path = AssetDatabase.GetAssetPath(obj.GetInstanceID());
        if (path.Length > 0)
        {
            if (Directory.Exists(path))
            {
                if (path.Contains("Resources") && path.Contains("Levels"))
                {
                    return true;
                }
            }
        }
        return false;
    }


    [MenuItem("FallingBalls/LevelEditor")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(LevelEditorWindow));
    }

    public LevelData levelData;
    public int levelNum;
    bool saved = false;
    public string PATH_TO_LEVELS;
    public ItemEditorPopup currentPopup;
    public EditorUtils textureUtil;
    private bool _switchTypeToBox;
    private int _defaultCapacity = 1;
    internal Vector2 _scrollPosition;
    private LineElementType _fillSelectedType;
    private int _fillCapacity;
    private int _linesToAdd;
    private int _seed;


    public void OnEnable()
    {
        if (textureUtil == null)
            textureUtil = Resources.Load<EditorUtils>("Editor/TypePairs");
        PATH_TO_LEVELS = Path.Combine(Application.dataPath, "Resources/Levels");
    }

    public void OnDestroy()
    {
        if (levelData != null && !saved)
        {
            if (EditorUtility.DisplayDialog("Save level?", "Do you want to save this level before clear editor?", "Save", "Clear"))
            {
                SaveLevel(levelNum);
                levelData = null;
            }
            else
            {
                levelData = null;
                return;
            }
        }
    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Level num: ");
        levelNum = EditorGUILayout.IntField(levelNum);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(levelData == null ? "Create Level" : "Save Level"))
        {
            if (levelData == null)
                levelData = new LevelData() { levelNum = levelNum };
            else
                SaveLevel(levelNum);
        }
        if (GUILayout.Button(levelData == null ? "Load Level" : "Clear Level"))
        {
            if (levelData == null)
            {
                if (LoadLevel(levelNum))
                {
                    Debug.Log("Loaded");
                }
                else
                {
                    if (EditorUtility.DisplayDialog("Load level failed", "Load level failed, do you want to start new?", "Create", "Cancel"))
                    {
                        levelData = new LevelData();
                    }
                    else
                    {
                        return;
                    }
                }
            }
            else
            {
                if (!saved)
                {
                    if (EditorUtility.DisplayDialog("Save level?", "Do you want to save this level before clear editor?", "Save", "Clear"))
                    {
                        SaveLevel(levelNum);
                        levelData = null;
                    }
                    else
                    {
                        levelData = null;
                        return;
                    }
                }
                else
                {
                    levelData = null;
                }
            }
        }
        GUILayout.EndHorizontal();
        string label = levelData == null ? "Empty" : (saved ? "Saved!" : "Not Saved");
        GUI.color = levelData == null ? Color.cyan : (saved ? Color.blue : Color.red);
        GUILayout.Label(label, EditorStyles.largeLabel);
        GUI.color = Color.white;
        GUILayout.Space(10);
        if (levelData != null)
        {
            float width = (Screen.width - 12) / 11;
            int c = levelData.lines.Count;
            if (c > 1)
            {
                GUILayout.BeginHorizontal();
                if (_switchTypeToBox)
                {
                    GUILayout.Label("Default capacity:");
                    _defaultCapacity = EditorGUILayout.IntField(_defaultCapacity);
                }
                if (GUILayout.Button(_switchTypeToBox ? "→ selection" : "← boxing"))
                {
                    _switchTypeToBox = !_switchTypeToBox;
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.Space(10);
            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, false, false, GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.box);
            for (int i = 0; i < c; i++)
            {
                GUILayout.BeginHorizontal();
                int l = levelData.lines[i].capacity.Count;
                for (int j = 0; j < l; j++)
                {
                    GUIContent content = new GUIContent();
                    switch (levelData.lines[i].capacity[j].elementType)
                    {
                        case LineElementType.Empty:
                            content.text = "E";
                            break;
                        case LineElementType.Box:
                            content.text = $"{levelData.lines[i].capacity[j].capacity}";
                            break;
                        case LineElementType.AddBall:
                            content.image = textureUtil.GetTexture(levelData.lines[i].capacity[j].elementType);
                            break;
                        case LineElementType.Randomize:
                            content.text = "R";
                            break;
                        case LineElementType.LinearKiller_H:
                            content.image = textureUtil.GetTexture(levelData.lines[i].capacity[j].elementType);
                            break;
                        case LineElementType.LinearKiller_V:
                            content.image = textureUtil.GetTexture(levelData.lines[i].capacity[j].elementType);
                            break;
                        case LineElementType.LinearKiller_X:
                            content.image = textureUtil.GetTexture(levelData.lines[i].capacity[j].elementType);
                            break;
                        case LineElementType.SquareKiller:
                            content.text = "○";
                            break;
                        case LineElementType.LinearKiller_H_B:
                            content.text = "○";
                            break;
                        case LineElementType.LinearKiller_V_B:
                            content.text = "○";
                            break;
                        case LineElementType.LinearKiller_X_B:
                            content.text = "○";
                            break;
                        case LineElementType.SquareKiller_B:
                            content.text = "○";
                            break;
                        default:
                            content.text = "○";
                            break;
                    }
                    GUIStyle _currentStyle = levelData.lines[i].capacity[j].elementType == LineElementType.Box ? textureUtil._boxStyle : textureUtil._emptyStyle;
                    if (GUILayout.Button(content, _currentStyle,
                        GUILayout.Width(width - 1),
                        GUILayout.Height(width - 1)))
                    {
                        if (!_switchTypeToBox)
                        {
                            OpenPopup(levelData.lines[i].capacity[j]);
                        }
                        else
                        {
                            if (levelData.lines[i].capacity[j].elementType == LineElementType.Box)
                            {
                                levelData.lines[i].capacity[j].elementType = LineElementType.Empty;
                                levelData.lines[i].capacity[j].capacity = 0;
                            }
                            else
                            {
                                levelData.lines[i].capacity[j].elementType = LineElementType.Box;
                                levelData.lines[i].capacity[j].capacity = _defaultCapacity;
                            }
                        }
                        saved = false;
                    }
                }
                if (GUILayout.Button("X", GUILayout.Width(width - 1), GUILayout.Height(width - 1)))
                {
                    levelData.lines.RemoveAt(i);
                    saved = false;
                    break;
                }
                GUILayout.EndHorizontal();
            }
            if (GUILayout.Button("AddLine"))
            {
                levelData.lines.Add(new LinesData(levelData.lines.Count));
                saved = false;
            }
            GUILayout.BeginHorizontal();
            _linesToAdd = EditorGUILayout.IntField("Lines: ", _linesToAdd);
            if (GUILayout.Button("Add lines"))
            {
                for (int i = 0; i < _linesToAdd; i++)
                {
                    levelData.lines.Add(new LinesData(levelData.lines.Count));
                }
                _linesToAdd = 0;
                saved = false;
            }
            GUILayout.EndHorizontal();
            GUILayout.EndScrollView();

            GUILayout.BeginVertical();
            GUILayout.Space(5);
            GUILayout.Box("", GUILayout.Height(8), GUILayout.ExpandWidth(true));
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            _fillSelectedType = (LineElementType)EditorGUILayout.EnumPopup("Fill buy:", _fillSelectedType);
            switch (_fillSelectedType)
            {
                case LineElementType.Box:
                    _fillCapacity = EditorGUILayout.IntField("Capacity:", _fillCapacity);
                    break;
                case LineElementType.AddBall:
                    _fillCapacity = EditorGUILayout.IntField("Capacity:", _fillCapacity);
                    break;
                case LineElementType.LinearKiller_H_B:
                    _fillCapacity = EditorGUILayout.IntField("Capacity:", _fillCapacity);
                    break;
                case LineElementType.LinearKiller_V_B:
                    _fillCapacity = EditorGUILayout.IntField("Capacity:", _fillCapacity);
                    break;
                case LineElementType.LinearKiller_X_B:
                    _fillCapacity = EditorGUILayout.IntField("Capacity:", _fillCapacity);
                    break;
                case LineElementType.SquareKiller_B:
                    _fillCapacity = EditorGUILayout.IntField("Capacity:", _fillCapacity);
                    break;
            }
            GUILayout.EndHorizontal();
            if (GUILayout.Button("Fill whole field"))
            {
                bool displayCapacity = _fillSelectedType == LineElementType.Box ||
                    _fillSelectedType == LineElementType.AddBall ||
                    _fillSelectedType == LineElementType.LinearKiller_H_B ||
                    _fillSelectedType == LineElementType.LinearKiller_V_B ||
                    _fillSelectedType == LineElementType.LinearKiller_X_B ||
                    _fillSelectedType == LineElementType.SquareKiller_B;
                if (EditorUtility.DisplayDialog("Fill field",
                    $"Are you sure, that you want to fill whole field by:\n{_fillSelectedType.ToString("f")}{(displayCapacity ? $" : {_fillCapacity}" : "")}",
                    "Fill", "Cancel"))
                {
                    FillField(_fillSelectedType, _fillCapacity, displayCapacity);
                    saved = false;
                }
            }
            //
            GUILayout.BeginHorizontal();
            levelData.maxScore = EditorGUILayout.IntField("Max score on level: ", levelData.maxScore);
            if (GUILayout.Button("CalcScore"))
            {
                CalcMaxScore();
            }
            GUILayout.EndHorizontal();
            //
            GUILayout.BeginVertical();
            GUILayout.Label("Reward", EditorStyles.boldLabel);
            //Coins
            GUILayout.BeginHorizontal();
            levelData.reward.coins = EditorGUILayout.IntField("Reward in coins: ", levelData.reward.coins);
            GUILayout.EndHorizontal();
            //Boosters
            GUILayout.BeginHorizontal();
            GUILayout.Label("Booster1:", GUILayout.Width(80));
            levelData.reward.booster1 = EditorGUILayout.IntField(levelData.reward.booster1, GUILayout.Width(50));
            GUILayout.Label("Booster2:", GUILayout.Width(80));
            levelData.reward.booster2 = EditorGUILayout.IntField(levelData.reward.booster2, GUILayout.Width(50));
            GUILayout.Label("Booster3:", GUILayout.Width(80));
            levelData.reward.booster3 = EditorGUILayout.IntField(levelData.reward.booster3, GUILayout.Width(50));
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            //
            GUILayout.Space(5);
            GUILayout.Label("Linear Random");
            GUILayout.BeginHorizontal();
            _fillCapacity = EditorGUILayout.IntField("Start capacity:", _fillCapacity);
            if (GUILayout.Button("Random build"))
            {
                if (EditorUtility.DisplayDialog("RandomBuild", $"Are you sure? it will rebuild all field", "Randomize", "Cancel"))
                {
                    RandomizeFieldLinear();
                }
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            GUILayout.Label("Perlin random");
            GUILayout.BeginHorizontal();
            _seed = EditorGUILayout.IntField("Seed:", _seed);
            if (GUILayout.Button("Random seed"))
            {
                _seed = UnityEngine.Random.Range(0, 2147483647);
            }
            if (GUILayout.Button("Randomize"))
            {
                if (EditorUtility.DisplayDialog("RandomBuild", $"Are you sure? it will rebuild all field", "Randomize", "Cancel"))
                {
                    RandomizeFieldPerlin();
                }
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

        }
        GUILayout.BeginVertical();
        GUILayout.Space(5);
        GUILayout.Box("", GUILayout.Height(8), GUILayout.ExpandWidth(true));
        textureUtil = (EditorUtils)EditorGUILayout.ObjectField(textureUtil, typeof(EditorUtils));
        GUILayout.EndVertical();

    }

    private void CalcMaxScore()
    {
        int c = 0;
        if (levelData.lines.Count >= 9)
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (levelData.lines[i].capacity[j].elementType == LineElementType.Box)
                    {
                        c++;
                    }
                }
            }
        }
        int r = 0;
        c = Mathf.CeilToInt(c * .8f);
        for (int i = 1; i <= c; i++)
        {
            r += i * 10;
        }
        levelData.maxScore = r;
    }

    private void RandomizeFieldLinear()
    {

        int c = levelData.lines.Count;
        for (int i = 0; i < c; i++)
        {
            int cl = levelData.lines[i].capacity.Count;
            for (int k = 0; k < cl; k++)
            {
                levelData.lines[i].capacity[k].elementType = LineElementType.Empty;
                levelData.lines[i].capacity[k].capacity = 0;
            }
            int r = UnityEngine.Random.Range(4, levelData.lines[i].capacity.Count + 1);
            for (int j = 0; j < r; j++)
            {
                int v = UnityEngine.Random.Range(0, levelData.lines[i].capacity.Count);
                levelData.lines[i].capacity[v].elementType = LineElementType.Box;
                if (levelData.lines[i].capacity[v].capacity >= (_fillCapacity + i) * 2)
                {
                    j--;
                    continue;
                }
                else
                {
                    levelData.lines[i].capacity[v].capacity += _fillCapacity + i;
                }
            }
        }
        saved = false;
    }

    private void RandomizeFieldPerlin()
    {
        int c = levelData.lines.Count;
        for (int i = 0; i < c; i++)
        {
            int e = levelData.lines[i].capacity.Count;
            for (int j = 0; j < e; j++)
            {
                float x = ((float)_seed / 100000000f) + ((float)i / 5f);
                float y = ((float)_seed / 100000000f) + ((float)j / 5f);
                float v = Mathf.PerlinNoise(x, y);
                v -= 0.3f;
                if (v >= 0.1f)
                {
                    levelData.lines[i].capacity[j].elementType = LineElementType.Box;
                    levelData.lines[i].capacity[j].capacity = i + (int)Mathf.Ceil(((_fillCapacity + i) * v * 2));
                }
                else
                {
                    levelData.lines[i].capacity[j].elementType = LineElementType.Empty;
                    levelData.lines[i].capacity[j].capacity = 0;
                }
            }
        }
        saved = false;
    }

    private void FillField(LineElementType fillSelectedType, int fillCapacity, bool displayCapacity)
    {
        int c = levelData.lines.Count;
        for (int i = 0; i < c; i++)
        {
            int e = levelData.lines[i].capacity.Count;
            for (int j = 0; j < e; j++)
            {
                levelData.lines[i].capacity[j].elementType = fillSelectedType;
                if (displayCapacity)
                    levelData.lines[i].capacity[j].capacity = fillCapacity;
                else
                    levelData.lines[i].capacity[j].capacity = 0;
            }
        }
        saved = false;
    }

    private void OpenPopup(FieldItemData fieldItemData)
    {
        if (currentPopup != null)
        {
            currentPopup.Close();
            currentPopup = null;
        }
        currentPopup = CreateInstance<ItemEditorPopup>();
        currentPopup.position = new Rect(GUIUtility.GUIToScreenPoint(Event.current.mousePosition), new Vector2(250, 80));
        currentPopup.Init(fieldItemData);
        currentPopup.ShowPopup();
    }

    private bool LoadLevel(int levelNum)
    {
        if (LevelExist(levelNum))
        {
            TextAsset asset = Resources.Load<TextAsset>($"Levels/level_{levelNum}");
            levelData = JsonUtility.FromJson<LevelData>(asset.text);
            if (levelData != null)
                return true;
        }
        return false;
    }

    private void SaveLevel(int levelNum)
    {
        if (LevelExist(levelNum))
        {
            if (!EditorUtility.DisplayDialog("Level exist", "That level number already exist!\nDo you want to override it?", "Yes", "Cancel"))
            {
                return;
            }
        }
        levelData.levelNum = levelNum;
        levelData.maxValue = GetMaxValue();
        string data = JsonUtility.ToJson(levelData);
        File.WriteAllText(Path.Combine(PATH_TO_LEVELS, $"level_{levelNum}.txt"), data);
        saved = true;
        AssetDatabase.Refresh();
    }

    private int GetMaxValue()
    {
        int x = 0;
        int c = levelData.lines.Count;
        for (int i = 0; i < c; i++)
        {
            int e = levelData.lines[i].capacity.Count;
            for (int j = 0; j < e; j++)
            {
                if (x < levelData.lines[i].capacity[j].capacity)
                {
                    x = levelData.lines[i].capacity[j].capacity;
                }
            }
        }
        return x;
    }

    private bool LevelExist(int levelNum)
    {
        return File.Exists(Path.Combine(PATH_TO_LEVELS, $"level_{levelNum}.txt"));
    }
}

public class ItemEditorPopup : EditorWindow
{
    public FieldItemData _itemToEdit;
    public void Init(FieldItemData itemToEdit)
    {
        _itemToEdit = itemToEdit;
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal(GUILayout.MaxWidth(250));
        GUILayout.Label("Element Type:", GUILayout.Width(100));
        _itemToEdit.elementType = (LineElementType)EditorGUILayout.EnumPopup(_itemToEdit.elementType, GUILayout.Width(150));
        GUILayout.EndHorizontal();
        switch (_itemToEdit.elementType)
        {
            case LineElementType.Empty:
                break;
            case LineElementType.Box:
                GUILayout.BeginHorizontal();
                GUILayout.Label("Capacity:", GUILayout.Width(100));
                _itemToEdit.capacity = EditorGUILayout.IntField(_itemToEdit.capacity, GUILayout.Width(150));
                GUILayout.EndHorizontal();
                break;
            case LineElementType.AddBall:
                GUILayout.BeginHorizontal();
                GUILayout.Label("Count:", GUILayout.Width(100));
                _itemToEdit.capacity = EditorGUILayout.IntField(_itemToEdit.capacity, GUILayout.Width(150));
                GUILayout.EndHorizontal();
                break;
            case LineElementType.Randomize:
                break;
            case LineElementType.LinearKiller_H:
                break;
            case LineElementType.LinearKiller_V:
                break;
            case LineElementType.LinearKiller_X:
                break;
            case LineElementType.SquareKiller:
                break;
            case LineElementType.LinearKiller_H_B:
                GUILayout.BeginHorizontal();
                GUILayout.Label("Capacity:", GUILayout.Width(100));
                _itemToEdit.capacity = EditorGUILayout.IntField(_itemToEdit.capacity, GUILayout.Width(150));
                GUILayout.EndHorizontal();
                break;
            case LineElementType.LinearKiller_V_B:
                GUILayout.BeginHorizontal();
                GUILayout.Label("Capacity:", GUILayout.Width(100));
                _itemToEdit.capacity = EditorGUILayout.IntField(_itemToEdit.capacity, GUILayout.Width(150));
                GUILayout.EndHorizontal();
                break;
            case LineElementType.LinearKiller_X_B:
                GUILayout.BeginHorizontal();
                GUILayout.Label("Capacity:", GUILayout.Width(100));
                _itemToEdit.capacity = EditorGUILayout.IntField(_itemToEdit.capacity, GUILayout.Width(150));
                GUILayout.EndHorizontal();
                break;
            case LineElementType.SquareKiller_B:
                GUILayout.BeginHorizontal();
                GUILayout.Label("Capacity:", GUILayout.Width(100));
                _itemToEdit.capacity = EditorGUILayout.IntField(_itemToEdit.capacity, GUILayout.Width(150));
                GUILayout.EndHorizontal();
                break;
            default:
                break;
        }
        if (GUILayout.Button("Ok")) Close();
    }

    private void OnLostFocus()
    {
        Close();
    }
}
