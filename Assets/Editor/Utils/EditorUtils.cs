﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "newEditorUtil", menuName = "Utils/EditorUtils/Textures")]
public class EditorUtils : ScriptableObject
{
    public List<LineElementTypeTexturePair> lineElementTypes = new List<LineElementTypeTexturePair>();
    public GUIStyle _boxStyle;
    public GUIStyle _emptyStyle;



    internal Texture GetTexture(LineElementType elementType)
    {
        return lineElementTypes.First(x => x.elementType == elementType).texture;
    }
}

[System.Serializable]
public class LineElementTypeTexturePair
{
    public LineElementType elementType;
    public Texture texture;
}