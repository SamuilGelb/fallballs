﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour
{
    public List<Transform> _targets = new List<Transform>();
    private Rigidbody _rigidbody;
    public Rigidbody Rigidbody
    {
        get
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();
            return _rigidbody;
        }
    }
    public float _waitTime = 3f;

    private Transform _currentTarget;
    private int _currentTargetNum = -1;

    public void Start()
    {
        _currentTarget = GetNextTarget();
    }

    private Transform GetNextTarget()
    {
        _currentTargetNum++;
        if (_currentTargetNum >= _targets.Count)
            _currentTargetNum = 0;
        return _targets[_currentTargetNum];
    }

    public void Update()
    {
        if (_currentTarget == null) return;
        var rot = Quaternion.Slerp(transform.localRotation, Quaternion.LookRotation(_currentTarget.position - transform.position), Time.deltaTime * 2f).eulerAngles;
        transform.rotation = Quaternion.Euler(0, rot.y, 0);
        Rigidbody.velocity = transform.forward * 2f;
        if (Vector3.Distance(transform.position, _currentTarget.position) < 2)
        {
            AwaitNextPoint();
            StartCoroutine(AwaitNextPoint());
        }
    }

    private IEnumerator AwaitNextPoint()
    {
        _currentTarget = null;
        yield return new WaitForSeconds(_waitTime);
        if (_currentTargetNum + 1 >= _targets.Count)
            yield break;
        _currentTarget = GetNextTarget();
    }
}
