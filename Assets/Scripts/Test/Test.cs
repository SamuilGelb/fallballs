﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Test : MonoBehaviour
{
    public Button _button;

    public void Awake()
    {
        _button.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        _button.interactable = false;
        StartCoroutine(SizeUpButton());
    }

    private IEnumerator SizeUpButton()
    {
        float newSize = (_button.transform as RectTransform).sizeDelta.x * 1.5f;
        while((_button.transform as RectTransform).sizeDelta.x < newSize)
        {
            (_button.transform as RectTransform).sizeDelta *= 1f + Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        _button.interactable = true;
    }
}