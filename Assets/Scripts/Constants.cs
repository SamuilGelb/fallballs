public static class PlayerPrefsConstants
{
    public static string ID = "ID";
    public static string NickName = "NickName";
    public static string RealName = "RealName";
    public static string EMail = "EMail";
    public static string Sex = "Sex";
    public static string BirthDay = "BirthDay";
    public static string RegestrationDate = "RegestrationDate";
    public static string LastOnline = "LastOnline";
    public static string Avatar = "Avatar";
    public static string ReferalCode = "ReferalCode";
    public static string Money = "GameMoney";
    public static string Level = "AccountLevel";
    public static string IsMusicOn = "IsMusicOn";
    public static string IsSoundOn = "IsSoundOn";
    public static string LikeOnSocial = "LikeOnSocial";
    public static string Boosters = "Boosters";
    public static string PassedLevels = "PassedLevels";
}

[System.Serializable]
public class LevelListData
{
    public int Count;
    public System.Collections.Generic.List<int> Levels;
}
