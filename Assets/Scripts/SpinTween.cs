using UnityEngine;

public class SpinTween : MonoBehaviour
{
    void Start()
    {
        StartSpin();
    }

    void StartSpin()
    {
        LeanTween.value(0, 360, 1.75f).setOnUpdate((float value) =>
        {
            if (this!= null && this.transform != null)
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, -value));
        }).setOnComplete(() =>
        {
            if (this != null && this.transform != null)
                StartSpin();
        });
    }
}
