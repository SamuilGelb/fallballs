using UnityEngine;

[System.Serializable]
public abstract class BaseBoosterData : ScriptableObject
{
    public Sprite Image;
    public int Count;
    public int Price;
    public bool SingleUsePerMove;
    public abstract BoosterType Type { get; }
}

public enum BoosterType
{
    BallsX2,
    DamageOnMap,
    SpawnItems
}