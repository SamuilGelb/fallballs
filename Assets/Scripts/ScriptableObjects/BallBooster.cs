using UnityEngine;

[CreateAssetMenu(fileName = "BallsBoosterData", menuName = "Boosters/BallsBooster", order = -5)]
public class BallBooster : BaseBoosterData
{
    public int maxBallsLimit;
    public override BoosterType Type => BoosterType.BallsX2;
}
