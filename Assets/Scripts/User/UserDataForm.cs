using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserDataForm
{ 
    public string ID;
    public string NickName;
    public string RealName;
    public string EMail;
    public UserSex Sex; 
    public DateTime BirthDay;
    public DateTime RegestrationDate;
    public DateTime LastOnline;

    public int Avatar;
    public string ReferalCode;

    public int GameMoney;
    public int AccountLevel;
    public Settigns Settings;
    public List<int> Boosters = new List<int>();

    public List<PassedLevelData> _passedLevels = new List<PassedLevelData>();  
    public List<PassedLevelData> PassedLevels 
    { 
        get
        {
            if (_passedLevels == null)
                _passedLevels = new List<PassedLevelData>();
            return _passedLevels;
        }
        private set
        {
            _passedLevels = value;
        }
    }  

    public UserDataForm()
    {
        //ID = UnityEngine.Random.Range(0, 2500000);
        NickName = "";
        Sex = UserSex.Uknown;
        BirthDay = DateTime.Now;
        RegestrationDate = DateTime.Now;
        LastOnline = DateTime.Now;
        Boosters = new List<int>() { 0, 0, 0 };
        PassedLevels = new List<PassedLevelData>();
        Settings = new Settigns() { IsMusicOn = true, IsSoundOn = true, LikeOnSocial = false };
    }

    public enum UserSex
    {
        Uknown,
        Male,
        Female,
        Another
    }

    public class Settigns
    {
        public bool IsSoundOn;
        public bool IsMusicOn;
        public bool LikeOnSocial;

    }
}
