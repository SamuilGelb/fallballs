using TMPro;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelItemViewController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _levelNum;
    [SerializeField] private Image _levelLocked;
    [SerializeField] private Image _levelDone;
    [SerializeField] private Image _oneStar;
    [SerializeField] private Image _twoStars;
    [SerializeField] private Image _threeStars;
    [SerializeField] private Button _levelStartButton;

    private PassedLevelData _passedLevelData;
    private int _level = 0;
    private Image _self;

    public void Awake()
    {
        _self = GetComponent<Image>();
    }

    public void Init(PassedLevelData passedLevelData)
    {
        _passedLevelData = passedLevelData;
        _self.color = Color.green;
        _levelNum.text = passedLevelData.LevelNum.ToString();
        _levelLocked.gameObject.SetActive(false);
        _levelDone.gameObject.SetActive(true);
        switch (passedLevelData.Stars)
        {
            case 1:
                _oneStar.gameObject.SetActive(true);
                _twoStars.gameObject.SetActive(false);
                _threeStars.gameObject.SetActive(false);
                break;
            case 2:
                _oneStar.gameObject.SetActive(true);
                _twoStars.gameObject.SetActive(true);
                _threeStars.gameObject.SetActive(false);
                break;
            case 3:
                _oneStar.gameObject.SetActive(true);
                _twoStars.gameObject.SetActive(true);
                _threeStars.gameObject.SetActive(true);
                break;
            default:
                _oneStar.gameObject.SetActive(false);
                _twoStars.gameObject.SetActive(false);
                _threeStars.gameObject.SetActive(false);
                break;
        }
        _levelStartButton.onClick.AddListener(OpenPopup);
    }

    private void OpenPopup()
    {
        throw new NotImplementedException();
    }

    public void Init(int levelNum, bool locked)
    {
        _level = levelNum;
        _levelNum.text = levelNum.ToString();
        _levelLocked.gameObject.SetActive(locked);
        _self.color = locked ? Color.red : Color.grey;
        _levelDone.gameObject.SetActive(false);
        _levelStartButton.onClick.AddListener(StartLevel);
    }

    private void StartLevel()
    {
        Container.Get<UserDataController>()._levelToStart = _level;
        SceneManager.LoadSceneAsync("GameScene");
    }
}
