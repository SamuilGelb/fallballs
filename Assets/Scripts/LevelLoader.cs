﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class LevelLoader : MonoBehaviour
{
    public GameObject LoadingScreen;
    public Slider slider;
    public TextMeshProUGUI ProgressText;

    private void Start()
    {
        StartCoroutine(LoadMainMenu());
    }
    IEnumerator LoadMainMenu()
    {
        LoadingScreen.SetActive(true);

        while (slider.value < 16)
        {
            yield return new WaitForSeconds(0.1f);
            slider.value += 1;
            ProgressText.text = $"{slider.value * 6.2f}%";
            
        } 
        SceneManager.LoadScene("Mainmenu");

    }
    

  
}
