using UnityEngine;

public class PopupShowExample : MonoBehaviour
{
    public RectTransform _transform => transform as RectTransform;

    void Start()
    {
        LeanTween.value(gameObject, Vector3.zero, Vector3.one * 1.1f, 0.3f).setOnUpdate((Vector3 value) =>
          {
              _transform.localScale = value;
          }).setOnComplete(() =>
          {
              LeanTween.value(gameObject, Vector3.one * 1.1f, Vector3.one, 0.1f).setOnUpdate((Vector3 value) =>
              {
                  _transform.localScale = value;
              })
              .setOnComplete(() =>
              {
                  Debug.Log("Hello world");
              });
          });
    }

}
