using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBase : MonoBehaviour
{
    public Sprite ToTest;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            PopupContainer.Get<DynamicPopupController>().Init<DynamicPopupData>(
                new DynamicPopupData()
                {
                    Header = "Hello there",
                    Image = ToTest,
                    Description = "Some description!",
                    twoButtons = false,
                    canSkip = true,
                    AcceptButtonText = "General Kenobi!?",
                    OnAccept = () => { Debug.Log("He accepted"); },
                    OnClose = () => { Debug.Log("Lya ti krisa!"); }
                }) ;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            PopupContainer.Get<DynamicPopupController>().Init<DynamicPopupData>(
                new DynamicPopupData()
                {
                    Header = "Go to cinema",
                    Description = "Ne hodi v kino na uncharted! tam Tom Holand, on shkal`nik",
                    twoButtons = true,
                    canSkip = false,
                    CancelButtonText = "Puk Puk Chelovek pauk",
                    AcceptButtonText = "Nathan Fillion forever",
                    OnAccept = () => { Debug.Log("Yeee boiii"); },
                    OnClose = () => { Debug.Log("Nu i zrya"); }
                });
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
        }

    }
}
