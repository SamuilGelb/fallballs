using TMPro;
using System;
using UnityEngine;
using UnityEngine.UI;

public class DynamicPopupController : PopupBase
{
    [SerializeField] private TextMeshProUGUI _headerText;
    [SerializeField] private Image _contentImage;
    [SerializeField] private TextMeshProUGUI _contentText;
    [SerializeField] private Button _cancelButton;
    [SerializeField] private Button _acceptButton;
    [SerializeField] private Button _closeButton;

    [SerializeField] private Action _cancelAction;
    [SerializeField] private Action _acceptAction;
    [SerializeField] private Action _closeAction;

    public void Awake()
    {
        _cancelButton.onClick.AddListener(() => { _cancelAction?.Invoke(); Hide(); });
        _acceptButton.onClick.AddListener(() => { _acceptAction?.Invoke(); Hide(); });
        _closeButton.onClick.AddListener(() => { _closeAction?.Invoke(); Hide(); });
    }

    public override void Init<T>(T obj)
    {
        var popupData = obj as DynamicPopupData;

        _headerText.text = popupData.Header;

        _contentImage.gameObject.SetActive(popupData.Image != null);
        if (popupData.Image != null)
            _contentImage.sprite = popupData.Image;

        _contentText.gameObject.SetActive(!string.IsNullOrEmpty(popupData.Description));
        if (!string.IsNullOrEmpty(popupData.Description))
            _contentText.text = popupData.Description;

        _cancelButton.gameObject.SetActive(popupData.twoButtons);
        if (popupData.twoButtons)
            _cancelButton.GetComponentInChildren<TextMeshProUGUI>().text = popupData.CancelButtonText ?? "Cancel";

        _acceptButton.GetComponentInChildren<TextMeshProUGUI>().text = popupData.AcceptButtonText ?? "Accept";

        _closeButton.gameObject.SetActive(popupData.canSkip);

        _cancelAction = popupData.OnCancel;
        _acceptAction = popupData.OnAccept;
        _closeAction = popupData.OnClose;

        Show();
    }

    protected override void AddToContainer()
    {
        PopupContainer.Add<DynamicPopupController>(this);
    }

    protected override void RemoveFromContainer()
    {
        PopupContainer.Remove<DynamicPopupController>();
    }
}
