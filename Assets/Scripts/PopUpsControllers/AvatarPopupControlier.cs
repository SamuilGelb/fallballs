using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class AvatarPopupControlier : PopupBase
{
    [SerializeField] private Image _contentImage;
    [SerializeField] private Button _CloseButton;
    [SerializeField] private Button _acceptButton;

    [SerializeField] private Action _CloseAction;
    [SerializeField] private Action _acceptAction;

    [SerializeField]private List<Image> ChooseAvatar = new List<Image>();

    public void Awake()
    {
        _CloseButton.onClick.AddListener(() => { _CloseAction?.Invoke(); Hide(); });
        _acceptButton.onClick.AddListener(() => { _acceptAction?.Invoke(); Hide(); });
    }

    public override void Init<T>(T obj)
    {
        var popupData = obj as DynamicPopupData;

        _contentImage.gameObject.SetActive(popupData.Image != null);
        if (popupData.Image != null)
            _contentImage.sprite = popupData.Image;

        
        _CloseButton.gameObject.SetActive(popupData.twoButtons);
        if (popupData.twoButtons)
            _CloseButton.GetComponentInChildren<TextMeshProUGUI>().text = popupData.CancelButtonText ?? "Cancel";

        _acceptButton.GetComponentInChildren<TextMeshProUGUI>().text = popupData.AcceptButtonText ?? "Accept";


        _CloseAction = popupData.OnCancel;
        _acceptAction = popupData.OnAccept;

        Show();
    }




    protected override void AddToContainer()
    {
        throw new NotImplementedException();
    }

    protected override void RemoveFromContainer()
    {
        throw new NotImplementedException();
    }

    
}
