using System;
using UnityEngine;

public class DynamicPopupData 
{
    public string Header = null;
    public Sprite Image = null;
    public string Description = null;
    public string CancelButtonText = null;
    public string AcceptButtonText = null;

    public bool twoButtons = true;
    public bool canSkip = false;

    public Action OnCancel;
    public Action OnClose;
    public Action OnAccept;
}
