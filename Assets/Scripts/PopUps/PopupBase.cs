using System;
using UnityEngine;

public abstract class PopupBase : MonoBehaviour
{
    [SerializeField] protected RectTransform _content;
    public virtual bool IsAnimationOnGoing { get; protected set; }

    public virtual event Action OnAnimationEnd;

    public virtual void Start()
    {
        AddToContainer();
    }

    protected abstract void AddToContainer();

    protected abstract void RemoveFromContainer();

    public virtual void Init<T>(T obj) where T : class
    {

    }

    public virtual void OnDestroy()
    {
        RemoveFromContainer();
    }

    public virtual void Show(bool immediately = false)
    {
        if (immediately)
        {
            _content.gameObject.SetActive(true);
        }
        else
        {
            IsAnimationOnGoing = true;
            _content.gameObject.SetActive(true);
            _content.transform.localScale = Vector3.zero;
            LeanTween.value(0f, 1.15f, 0.35f).setOnUpdate((float value) =>
            {
                _content.transform.localScale = Vector3.one * value;
            }).setOnComplete(() =>
            {
                LeanTween.value(1.15f, 1f, 0.1f).setOnUpdate((float value) =>
                {
                    _content.transform.localScale = Vector3.one * value;
                }).setOnComplete(() =>
                {
                    IsAnimationOnGoing = false;
                    OnAnimationEnd?.Invoke();
                });
            });
        }
    }

    public virtual void Hide(bool immediately = false)
    {
        if (immediately)
        {
            _content.gameObject.SetActive(false);
        }
        else
        {
            IsAnimationOnGoing = true;
            _content.transform.localScale = Vector3.one;
            LeanTween.value(1f, 1.15f, 0.1f).setOnUpdate((float value) =>
            {
                _content.transform.localScale = Vector3.one * value;
            }).setOnComplete(() =>
            {
                LeanTween.value(1f, 0f, 0.35f).setOnUpdate((float value) =>
                {
                    _content.transform.localScale = Vector3.one * value;
                }).setOnComplete(() =>
                {
                    IsAnimationOnGoing = false;
                    OnAnimationEnd?.Invoke();
                    _content.gameObject.SetActive(false);
                });
            });
        }
    }
}
