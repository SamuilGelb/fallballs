using System;
using System.Collections.Generic;

public static class PopupContainer
{
    private static Dictionary<Type, PopupBase> _container = new Dictionary<Type, PopupBase>();

    public static void Add<T>(T controller) where T : PopupBase
    {
        _container.Add(controller.GetType(), controller);
    }

    public static bool Contains<T>() where T : PopupBase
    {
        return _container.ContainsKey(typeof(T));
    }

    public static T Get<T>() where T : PopupBase
    {
        if (_container.ContainsKey(typeof(T)))
        {
            PopupBase founded = _container[typeof(T)];
            return founded as T;
        }

        return null;
    }

    public static void Remove<T>() where T : PopupBase
    {
        if (_container.ContainsKey(typeof(T)))
        {
            _container.Remove(typeof(T));
        }
    }
}