﻿using UnityEngine;

public class LineBoosterBoth : LineBoosterBase
{
    [SerializeField] protected ScaleBaseTween _effectSecond;

    public new void Start()
    {
        LevelHolderController.Instance.MoveStarted += CheckForDie;
    }

    private new void CheckForDie()
    {
        if (_hasToDie && gameObject != null)
            Destroy(this.gameObject);
    }

    private new void OnDestroy()
    {
        LevelHolderController.Instance.MoveStarted -= CheckForDie;
    }

    protected new void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Ball")
        {
            _hasToDie = true;
            _effect.Show(() =>
            {
                LevelGenerator.Instance.TriggerBooster(transform.localPosition, _orientation);
                _effect.Hide(() => { Debug.Log("Hited"); });
            });
            _effectSecond.Show(true, () => { });
        }
    }
}