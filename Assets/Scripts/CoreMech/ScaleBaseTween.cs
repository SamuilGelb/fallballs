﻿using System;
using UnityEngine;

public class ScaleBaseTween : TweenEffectBase
{
    [SerializeField] Vector3 _baseScale;
    [SerializeField] Vector3 _targetScale;

    public override void Play()
    {
        base.Play();
        Show(true, () => { Reset(); });
    }

    public override void Reset()
    {
        transform.localScale = _baseScale;
        base.Reset();
    }

    public override void Stop()
    {
        base.Stop();
        Hide(()=> { Reset(); });
    }

    public override void Show(Action OnCompleate)
    {
        _anim = LeanTween.value(gameObject, _baseScale, _targetScale, _loopTime / 2).
             setOnUpdate((Vector3 value) =>
             {
                 transform.localScale = value;
             }).
             setOnComplete(() =>
             {
                 OnCompleate();
             });
    }

    public override void Hide(Action OnCompleate)
    {
        _anim = LeanTween.value(gameObject, _targetScale, _baseScale, _loopTime / 2).
             setOnUpdate((Vector3 value) =>
             {
                 transform.localScale = value;
             }).
             setOnComplete(() =>
             {
                 OnCompleate();
             });
    }
}
