﻿
using System.Collections.Generic;

[System.Serializable]
public class LevelData
{
    public int levelNum;
    public List<LinesData> lines = new List<LinesData>();
    public int maxValue;
    public int maxScore;
    public Reward reward;
}

[System.Serializable]
public class Reward
{
    public int coins;
    public int booster1;
    public int booster2;
    public int booster3;
}

[System.Serializable]
public class LinesData
{
    public string name = "";
    public List<FieldItemData> capacity = new List<FieldItemData>();

    public LinesData(int n)
    {
        name = $"{n}";
        capacity = new List<FieldItemData>()
        {
            new FieldItemData(1),
            new FieldItemData(2),
            new FieldItemData(3),
            new FieldItemData(4),
            new FieldItemData(5),
            new FieldItemData(6),
            new FieldItemData(7),
            new FieldItemData(8),
            new FieldItemData(9)
        };
    }
}

[System.Serializable]
public class FieldItemData
{
    public string name = "";
    public int capacity = 0;
    public LineElementType elementType = LineElementType.Empty;

    public FieldItemData(int num)
    {
        name = $"{num}";
    }
}

[System.Serializable]
public enum LineElementType
{
    Empty,
    Box,
    AddBall,
    Randomize,
    LinearKiller_H,
    LinearKiller_V,
    LinearKiller_X,
    SquareKiller,
    LinearKiller_H_B,
    LinearKiller_V_B,
    LinearKiller_X_B,
    SquareKiller_B,
}
