﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BallController : MonoBehaviour
{
    [SerializeField] Vector2 _startDirection = Vector2.zero;
    [SerializeField] float _startPower = 3f;
    [SerializeField] bool _punchAtStart = false;

    public delegate void Destruction();
    public event Destruction OnDestroyed;



    private Rigidbody2D _rigidbody;

    public Rigidbody2D Rigidbody
    {
        get
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody2D>();
            return _rigidbody;
        }
    }

    internal void CheckVelocity()
    {

        if (Mathf.Abs(Rigidbody.velocity.x) < 0.05f)
        {
            RandomDirection();
            Rigidbody.velocity += _startDirection;
        }
    }

    private void Awake()
    {
        if (_punchAtStart)
        {
            Rigidbody.velocity = _startDirection * _startPower;
        }
    }

    public void SetRandomVelocityOffset()
    {
        RandomDirection();
        Rigidbody.velocity = _startDirection;
    }

    public void OnDestroy()
    {
        OnDestroyed();
    }


    [ContextMenu("RandomizeDirection")]
    private void RandomDirection()
    {
        _startDirection = new Vector2(
            Random.Range(-0.2f, 0.22f),
            Random.Range(-0.5f, 0.5f));
    }

    internal void SetDirection(Vector2 direction)
    {
        Rigidbody.velocity = _startDirection + direction.normalized * 5f;
    }
}
