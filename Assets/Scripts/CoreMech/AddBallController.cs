﻿using UnityEngine;

public class AddBallController : LevelElementBase
{
    public override void Init(FieldItemData data)
    {
        _data = data;
        Health = 1;
    }

    public override void GetDamage(int count)
    {
        Debug.Log("Can't damage booster");
    }

    protected new void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Ball")
        {
            Destroy(gameObject);
        }
    }
}