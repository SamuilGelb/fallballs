﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelRestarter : MonoBehaviour
{
    public Button _restart;

    public void Awake()
    {
        _restart.onClick.AddListener(() => RestartLevel());
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
