﻿using TMPro;
using UnityEngine;

public class BoxController : LevelElementBase
{
    [SerializeField] protected TextMeshProUGUI _power;
    private int _maxValue;


    public new int Health
    {
        get { return _health; }
        private set
        {
            _health = value;
            _power.text = $"{value}";
            var hueValue = (float)value / (float)(_maxValue == 0 ? 10 : _maxValue);
            SpriteRenderer.color = Color.HSVToRGB(hueValue, 1, 1);
        }
    }

    public void Awake()
    {
        _power = GetComponentInChildren<TextMeshProUGUI>();
        if (_power != null)
            _power.text = $"{_health}";
    }

    public void Init(FieldItemData data, int maxValue)
    {
        _maxValue = maxValue;
        _health = data.capacity;
        _power.text = $"{_health}";
        SpriteRenderer.color = Color.HSVToRGB((float)data.capacity / (float)maxValue, 1, 1);
    }

    public override void GetDamage(int count)
    {
        Health -= count;
        if (Health <= 0)
        {
            Destroy(gameObject);
        }
    }

    protected new void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            GetDamage(1);
            collision.gameObject.GetComponent<BallController>().CheckVelocity();
        }
    }
}
