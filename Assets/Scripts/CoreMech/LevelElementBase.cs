﻿using System;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class LevelElementBase : MonoBehaviour
{
    [SerializeField] protected int _health = 10;
    protected FieldItemData _data;
    protected SpriteRenderer _spriteRenderer;
    public int Health
    {
        get { return _health; }
        set
        {
            _health = value;
        }
    }
    public SpriteRenderer SpriteRenderer
    {
        get
        {
            if (_spriteRenderer == null)
                _spriteRenderer = GetComponent<SpriteRenderer>();
            return _spriteRenderer;
        }
    }
    public Action<LevelElementBase> OnDestroyed;

    public virtual void Init(FieldItemData data)
    {
        _data = data;
    }
    public virtual void GetDamage(int count)
    {
        Health -= count;
        if (Health <= 0)
        {
            Destroy(gameObject);
        }
    }

    protected void OnDestroy()
    {
        if (OnDestroyed != null)
            OnDestroyed(this);
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {

    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        
    }
}
