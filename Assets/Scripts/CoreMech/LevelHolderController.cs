﻿using System.Linq;
using UnityEngine;

public class LevelHolderController : MonoBehaviour
{
    public static LevelHolderController Instance;

    private const float LINE_SIZE = 0.48f;
    private const float SHIFT_TIME = 0.5f;
    private int _currentLine = 0;
    public static bool _moveInProgress = false;

    private const float UPPER_BORDER = 3.7f;
    private const float LOWER_BORDER = -4.7f;

    public System.Action MoveStarted;
    public System.Action MoveFinished;

    public void Awake()
    {
        Instance = this;
    }

    [ContextMenu("MoveLevelUp")]
    private void MoveOneLine()
    {
        MoveUp(1);
    }

    public void MoveUp(int lines)
    {
        _moveInProgress = true;
        float _currentPosition = transform.position.y;
        if (MoveStarted != null)
            MoveStarted();
        LeanTween.value(gameObject,
            new Vector2(0, _currentPosition),
            new Vector2(0, _currentPosition + lines * LINE_SIZE),
            lines * SHIFT_TIME).
            setOnUpdate((Vector2 value) =>
            {
                transform.position = value;
            }).
            setOnComplete(() =>
            {
                _currentLine += lines;
                CheckChildren();
                _moveInProgress = false;
                if (MoveFinished != null)
                    MoveFinished();
                Debug.Log("Shifted");
            });
    }

    public void CheckChildren()
    {
        var childs = transform.GetComponentsInChildren<Collider2D>().ToList();
        var some = childs.Any(x => x.transform.position.y > UPPER_BORDER);
        if (some)
        {
            Debug.Log("Failed");
        }
        int c = childs.Count;
        for (int i = 0; i < c; i++)
        {
            childs[i].GetComponent<Collider2D>().enabled = false;
        }
        var another = childs.Where(x => x.transform.position.y > LOWER_BORDER).ToList();
        c = another.Count;
        for (int i = 0; i < c; i++)
        {
            another[i].GetComponent<Collider2D>().enabled = true;
        }
    }
}
