﻿using UnityEngine;

public class AngleBoxController : BoxController
{
    [SerializeField] float _rotationAngle;

    public void Init(FieldItemData data, int maxValue, float rotationAngle)
    {
        Init(data, maxValue);
        _rotationAngle = rotationAngle;
        transform.rotation = Quaternion.Euler(0, _rotationAngle, 0);
        _power.transform.parent.rotation = Quaternion.Euler(0, -_rotationAngle, 0);
    }
}
