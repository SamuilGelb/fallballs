﻿using System;
using UnityEngine;

public class GameManager : BaseController
{
    public static GameManager Instance;
    [SerializeField] private BallKiller _ballKiller;
    [SerializeField] private LevelGenerator _levelGenerator;
    [SerializeField] private LevelHolderController _levelHolderController;
    [SerializeField] private SpawnController _spawnController;
    [SerializeField] private LoadingController _loadingController;
    [SerializeField] private int _score;

    public BallKiller BallKiller => _ballKiller;
    public LevelGenerator LevelGenerator => _levelGenerator;
    public LevelHolderController LevelHolderController => _levelHolderController;
    public SpawnController SpawnController => _spawnController;
    public LoadingController LoadingController => _loadingController;
    public int Score => _score;

    public Action<int> OnScoreUpdated;

    [Space]
    [SerializeField] private int _currentLevel;

    public override void Awake()
    {
        base.Awake();
        Instance = this;
        SpawnController.FinishLevel += ShowWinPopup;
        _currentLevel = Container.Get<UserDataController>()._levelToStart;
        StartNewLevel();
    }

    private void ShowWinPopup()
    {
        Container.Get<UserDataController>().PassLevel(new PassedLevelData()
        {
            LastGameDate = DateTime.Now,
            LevelNum = _currentLevel,
            Score = Score,
            Stars = Score >= _levelGenerator.MaxScore ? 3 : Score >= _levelGenerator.MaxScore * 0.75f ? 2 : Score >= _levelGenerator.MaxScore * 0.5f ? 1 : 0
        }) ;
        Container.Get<UserDataController>().SaveData();
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("MainMenu");
    }

    public async void StartNewLevel()
    {
        _levelHolderController.transform.position = Vector2.zero;
        _spawnController._levelCleared = false;
        _levelGenerator._levelToLoad = _currentLevel;
        _spawnController._restoredBalls = 4;
        _spawnController._ballToSpawn = _spawnController._restoredBalls;
        await _levelGenerator.ClearLevel();
        await _levelGenerator.LoadLevel();
        LoadingController.Hide();
    }

    protected override void AddToContainer()
    {
        Container.Add<GameManager>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<GameManager>();
    }

    internal void GetScore(int score)
    {
        _score += score;
        OnScoreUpdated?.Invoke(Score);
    }
}
