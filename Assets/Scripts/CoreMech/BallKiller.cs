﻿using UnityEngine;

public class BallKiller : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Ball")
        {
            Destroy(collision.transform.gameObject);
        }
    }
}
