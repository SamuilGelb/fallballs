﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Threading.Tasks;

public class LevelGenerator : MonoBehaviour
{
    public static LevelGenerator Instance;

    public int _currentScoreMultipler = 1;
    public int _levelToLoad = 1;
    [SerializeField] private Transform _levelHolder;
    [SerializeField] private LevelData _levelData;
    [SerializeField] private BoxController _boxPrefab;
    [SerializeField] private AddBallController _addBallPrefab;
    [SerializeField] private LineBoosterBase _horizontalLinePrefab;
    [SerializeField] private LineBoosterBase _verticalLinePrefab;
    [SerializeField] private LineBoosterBase _bothLinePrefab;

    public int MaxScore => _levelData.maxScore;

    private const float _cellSize = 0.48f;
    private const float _spawnStartCordX = -1.92f;
    private const float _minGameHeight = -2.88f;
    private const int _scorePerDestroyedItem = 10;

    [Space]
    [SerializeField] private List<LevelElementBase> _spawnedItems = new List<LevelElementBase>();

    public Action LevelCleared;

    public void Awake()
    {
        Instance = this;
        _currentScoreMultipler = 1;
        //await LoadLevel();
        GameManager.Instance.SpawnController.LevelMove += OnLevelMoved;
    }

    private void OnLevelMoved()
    {
        _currentScoreMultipler = 1;
    }

    public async Task LoadLevel()
    {
        TextAsset level = Resources.Load<TextAsset>($"Levels/level_{_levelToLoad}");
        _levelData = JsonUtility.FromJson<LevelData>(level.text);
        await BuildLevel();
    }

    internal void TriggerBooster(Vector3 position, LineBoosterBase.LineBoosterOrient orientation)
    {
        switch (orientation)
        {
            case LineBoosterBase.LineBoosterOrient.Horizontal:
                DamageHorizontal(position.y);
                break;
            case LineBoosterBase.LineBoosterOrient.Vertical:
                DamageVertical(position.x);
                break;
            case LineBoosterBase.LineBoosterOrient.Both:
                DamageHorizontal(position.y);
                DamageVertical(position.x);
                break;
            default:
                DamageHorizontal(position.y);
                break;
        }
    }

    private void DamageVertical(float x)
    {
        var lineObj = _spawnedItems.FindAll(_x => _x.transform.localPosition.x == x);
        int c = lineObj.Count;
        for (int i = 0; i < c; i++)
        {
            lineObj[i].GetDamage(1);
        }
    }

    private void DamageHorizontal(float y)
    {
        List<LevelElementBase> lineObj = _spawnedItems.FindAll(x => (int)(x.transform.localPosition.y * 1000000) - (int)(y * 1000000) > -200
        && (int)(x.transform.localPosition.y * 1000000) - (int)(y * 1000000) < 200);
        int c = lineObj.Count;
        for (int i = 0; i < c; i++)
        {
            lineObj[i].GetDamage(1);
        }
    }

    private async Task BuildLevel()
    {
        await ClearLevel();
        int c = _levelData.lines.Count;
        for (int i = 0; i < c; i++)
        {
            await Task.Delay(5);
            int e = _levelData.lines[i].capacity.Count;
            for (int j = 0; j < e; j++)
            {
                await Task.Delay(2);
                switch (_levelData.lines[i].capacity[j].elementType)
                {
                    case LineElementType.Empty:
                        break;
                    case LineElementType.Box:
                        var box = Instantiate(_boxPrefab, _levelHolder);
                        box.transform.position = new Vector3(_spawnStartCordX + j * _cellSize, -i * _cellSize);
                        box.Init(_levelData.lines[i].capacity[j], _levelData.maxValue);
                        _spawnedItems.Add(box);
                        box.OnDestroyed += RemoveBox;
                        break;
                    case LineElementType.AddBall:
                        var aBall = Instantiate(_addBallPrefab, _levelHolder);
                        aBall.transform.position = new Vector3(_spawnStartCordX + j * _cellSize, -i * _cellSize);
                        aBall.Init(_levelData.lines[i].capacity[j]);
                        _spawnedItems.Add(aBall);
                        aBall.OnDestroyed += AddBall;
                        break;
                    case LineElementType.Randomize:
                        break;
                    case LineElementType.LinearKiller_H:
                        var lineH = Instantiate(_horizontalLinePrefab, _levelHolder);
                        lineH.transform.position = new Vector3(_spawnStartCordX + j * _cellSize, -i * _cellSize);
                        lineH.Init(_levelData.lines[i].capacity[j]);
                        lineH.OnDestroyed += RemoveElementFromList;
                        _spawnedItems.Add(lineH);
                        break;
                    case LineElementType.LinearKiller_V:
                        var lineV = Instantiate(_horizontalLinePrefab, _levelHolder);
                        lineV.transform.position = new Vector3(_spawnStartCordX + j * _cellSize, -i * _cellSize);
                        lineV.Init(_levelData.lines[i].capacity[j]);
                        lineV.OnDestroyed += RemoveElementFromList;
                        _spawnedItems.Add(lineV);
                        break;
                    case LineElementType.LinearKiller_X:
                        var lineX = Instantiate(_horizontalLinePrefab, _levelHolder);
                        lineX.transform.position = new Vector3(_spawnStartCordX + j * _cellSize, -i * _cellSize);
                        lineX.Init(_levelData.lines[i].capacity[j]);
                        lineX.OnDestroyed += RemoveElementFromList;
                        _spawnedItems.Add(lineX);
                        break;
                    case LineElementType.SquareKiller:
                        break;
                    case LineElementType.LinearKiller_H_B:
                        break;
                    case LineElementType.LinearKiller_V_B:
                        break;
                    case LineElementType.LinearKiller_X_B:
                        break;
                    case LineElementType.SquareKiller_B:
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void RemoveElementFromList(LevelElementBase obj)
    {
        _spawnedItems.Remove(obj);
    }

    private void AddBall(LevelElementBase obj)
    {
        RemoveElementFromList(obj);
        GameManager.Instance.SpawnController.AddBallToRestore(obj.Health);
    }

    private void RemoveBox(LevelElementBase ctrl)
    {
        if (ctrl != null)
        {
            GameManager.Instance.GetScore(_currentScoreMultipler * _scorePerDestroyedItem);
            _currentScoreMultipler++;
        }
        if (!_spawnedItems.Remove(ctrl as BoxController))
        {
            int c = _spawnedItems.Count;
            for (int i = 0; i < c; i++)
            {
                if (_spawnedItems[i] == null)
                {
                    _spawnedItems.RemoveAt(i);
                    i--;
                }
            }
        }
        if (_spawnedItems.Count == 0)
        {
            if (LevelCleared != null)
            {
                LevelCleared();
            }
        }
    }

    public async Task ClearLevel()
    {
        int c = _spawnedItems.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(_spawnedItems[i].gameObject);
            await Task.Delay(5);
        }
        _spawnedItems.Clear();
    }

    internal int GetLinesToMove()
    {
        while (_spawnedItems[0] == null)
            _spawnedItems.RemoveAt(0);
        if (_spawnedItems[0]?.transform?.position.y < _minGameHeight)
        {
            int l = Mathf.CeilToInt((_minGameHeight -
                _spawnedItems[0]?.transform.position.y ?? _minGameHeight) / _cellSize) + 1;
            return l;
        }
        return 1;
    }
}
