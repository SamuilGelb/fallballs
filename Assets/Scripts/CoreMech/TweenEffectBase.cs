﻿using System;
using UnityEngine;

public class TweenEffectBase : MonoBehaviour
{
    [Tooltip("Is animation played on awake")]
    [SerializeField] protected bool _playOnAwake = false;
    [Tooltip("Loop animation cycle")]
    [SerializeField] protected bool _loop = false;
    [Tooltip("0 - infinity")]
    [SerializeField] protected int _loopCount = 0;
    [Tooltip("Full cycle animation time")]
    [SerializeField] protected float _loopTime = 1f;
    protected bool _isPlaying;
    public bool IsPlaying { get => _isPlaying; }
    protected int _currentLoopCount = 0;
    protected LTDescr _anim;


    public void Awake()
    {
        _currentLoopCount = 0;
        if(_playOnAwake)
        {
            Show(true, Loop);
        }
    }

    private void Loop()
    {
        _currentLoopCount++;
        if (_loop)
        {
            if (_currentLoopCount < _loopCount)
            {
                Show(true, Loop);
            }
        }
    }

    public virtual void Reset()
    {
        _currentLoopCount = 0;
    }

    public virtual void Play()
    {
        _isPlaying = true;
    }

    public virtual void Stop()
    {
        _isPlaying = false;
    }

    public virtual void Show(bool hide, Action OnCompleate)
    {
        Show(() =>
        {
            if (hide)
                Hide(() =>
                {
                    if (OnCompleate != null)
                        OnCompleate();
                });
        });
    }

    public virtual void Show(Action OnCompleate)
    {

        if (OnCompleate != null)
            OnCompleate();
    }

    public virtual void Hide(Action OnCompleate)
    {

        if (OnCompleate != null)
            OnCompleate();
    }
}
