﻿using UnityEngine;

public class LineBoosterBase : LevelElementBase
{
    [SerializeField] protected LineBoosterOrient _orientation;
    [SerializeField] protected ScaleBaseTween _effect;
    [SerializeField] protected bool _hasToDie = false;

    public void Start()
    {
        LevelHolderController.Instance.MoveStarted += CheckForDie;
    }

    public override void GetDamage(int count)
    {
        Debug.Log("Can't damage booster");
    }

    protected void CheckForDie()
    {
        if (_hasToDie && gameObject != null)
            Destroy(this.gameObject);
    }
    private new void OnDestroy()
    {
        LevelHolderController.Instance.MoveStarted -= CheckForDie;
    }

    public override void Init(FieldItemData data)
    {
        base.Init(data);
        switch (data.elementType)
        {
            case LineElementType.LinearKiller_H:
                _orientation = LineBoosterOrient.Horizontal;
                break;
            case LineElementType.LinearKiller_V:
                _orientation = LineBoosterOrient.Vertical;
                break;
            case LineElementType.LinearKiller_X:
                _orientation = LineBoosterOrient.Both;
                break;
            default:
                _orientation = LineBoosterOrient.Horizontal;
                break;
        }
    }

    protected new void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Ball")
        {
            _hasToDie = true;
            _effect.Show(() =>
            {
                LevelGenerator.Instance.TriggerBooster(transform.localPosition, _orientation);
                _effect.Hide(() => { Debug.Log("Hited"); });
            });
        }
    }

    public enum LineBoosterOrient
    {
        Horizontal,
        Vertical,
        Both
    }
}
