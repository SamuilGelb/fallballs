﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField] BallController _ballPrefab;
    [SerializeField] public int _ballToSpawn = 10;
    [SerializeField] public int _restoredBalls = 7;
    [SerializeField]
    [Range(0.1f, 2f)]
    private float _spawnSpeed = 0.2f;
    private float _lastSpawnTime;
    private int _aliveBalls = 0;
    [SerializeField] SpriteRenderer _arrow;
    [SerializeField] SpriteRenderer _phantom;
    [SerializeField] TextMeshProUGUI _ballsCount;
    [SerializeField] Transform _ballsCountCanvas;
    [Space]
    [SerializeField] private Vector2 _startPoint;
    [SerializeField] private Vector2 _endPoint;
    [SerializeField] private LineRenderer _lineRender;

    [SerializeField] private float _ballRadius;
    [SerializeField] private float _wallPosition;
    public Action LevelMove;
    public Action FinishLevel;
    public bool _levelCleared;

    public void Start()
    {
        LevelGenerator.Instance.LevelCleared += LevelCleared;
        GameManager.Instance.LoadingController.OnHide += UpdateView;
    }

    private void UpdateView()
    {
        _ballsCount.text = $"x {_ballToSpawn}";
        _ballsCountCanvas.gameObject.SetActive(true);
    }

    private void LevelCleared()
    {
        _levelCleared = true;
    }

    public void AddBallToRestore(int c)
    {
        _restoredBalls += c;
    }

    public void Update()
    {
        if (_lastSpawnTime > Time.time
            || _ballToSpawn <= 0
            || LevelHolderController._moveInProgress) return;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit _hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out _hit, 100))
            {
                _lineRender.gameObject.SetActive(true);
                _arrow.color = Color.white;
                _arrow.transform.position = _startPoint;
                var newEndPoint = RoundVectors(new Vector3(_hit.point.x, _hit.point.y), 3);
                if (Vector2.Distance(newEndPoint, _endPoint) > 0.02f)
                {
                    BuildLine();
                    _endPoint = newEndPoint;
                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            RaycastHit _hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out _hit, 100))
            {
                var newEndPoint = RoundVectors(new Vector3(_hit.point.x, _hit.point.y), 3);
                if (Vector2.Distance(newEndPoint, _endPoint) > 0.02f)
                {
                    BuildLine();
                    _endPoint = newEndPoint;
                }
                Vector3 rotation = Quaternion.LookRotation(_endPoint - _startPoint).eulerAngles;
                Vector3 rightRotation = new Vector3(0, 0, (-rotation.x * (rotation.y > 180 ? -1 : 1)) + (rotation.y > 180 ? 180 : 0));
                _arrow.transform.rotation = Quaternion.Euler(rightRotation);
                _arrow.size = new Vector2(Mathf.Min(Vector2.Distance(_startPoint, _endPoint) + 0.1f, 1.2f), 0.128f);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit _hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out _hit, 100))
            {
                _lineRender.gameObject.SetActive(false);
                _arrow.color = Color.clear;
                if (_ballsCountCanvas.gameObject.activeSelf)
                    _ballsCountCanvas.gameObject.SetActive(false);
                StartCoroutine(SpawnBalls());
            }
        }
    }

    private void BuildLine()
    {
        List<Vector3> dots = new List<Vector3>();
        dots.Add(_startPoint);
        float _hitPosition = _wallPosition - _ballRadius;
        Vector3 direction = (RoundVectors(_endPoint, 3) - RoundVectors(_startPoint, 3));
        direction = direction.normalized * 5;
        direction = RoundVectors(direction, 3);
        Vector3 prevPoint = RoundVectors(_startPoint, 3);
        do
        {
            direction += RoundVectors(new Vector3(0, -9.8f) * 0.008333f, 3);
            Vector3 newDot = RoundVectors(prevPoint + direction * 0.008333f, 3);
            if (newDot.x >= _hitPosition)
            {
                float newY = prevPoint.y + (((newDot.x - _hitPosition) / (direction.x * 0.008333f)) * (newDot.y - prevPoint.y));
                newDot.Set(_hitPosition, newY, 0);
                newDot = RoundVectors(newDot, 3);
            }
            if (newDot.x <= -_hitPosition)
            {
                float newY = prevPoint.y + (((newDot.x + _hitPosition) / (direction.x * 0.008333f)) * (newDot.y - prevPoint.y));
                newDot.Set(-_hitPosition, newY, 0);
                newDot = RoundVectors(newDot, 3);
            }
            if (Mathf.Abs(newDot.x) >= _hitPosition)
                direction = RoundVectors(new Vector3(-direction.x * (direction.x > 0 ? 1 : 0.95f), direction.y, 0), 3);
            dots.Add(newDot);
            prevPoint = newDot;
        }
        while (Math.Abs(prevPoint.y) < 4.56f);
        int d = dots.Count / 50;
        if (dots.Count > 50)
        {
            dots.RemoveRange(d * 50, dots.Count - d * 50);
            dots = dots.Where((value, index) => index % d == 0).ToList();
        }
        _lineRender.positionCount = dots.Count;
        _lineRender.SetPositions(dots.ToArray());
    }

    private IEnumerator SpawnBalls()
    {
        while (_ballToSpawn > 0)
        {
            _lastSpawnTime = Time.time + _spawnSpeed;
            var createdBall = Instantiate(_ballPrefab, _startPoint, Quaternion.identity);
            var c_transform = createdBall.transform;
            c_transform.position = new Vector3(c_transform.position.x, c_transform.position.y, 0);
            createdBall.SetDirection(_endPoint - _startPoint);
            _ballToSpawn--;
            _aliveBalls++;
            createdBall.OnDestroyed += CreatedBall_OnDestroyed;
            yield return new WaitForSeconds(_spawnSpeed);
        }
        _phantom.color = Color.clear;
    }

    private async void CreatedBall_OnDestroyed()
    {
        _aliveBalls--;
        if (_aliveBalls <= 3)
            Time.timeScale = 2f;
        if (_aliveBalls == 1)
            Time.timeScale = 2.5f;
        _phantom.color = Color.white;
        await CheckForLevelMove();
    }

    private async Task CheckForLevelMove()
    {
        if (_ballToSpawn == 0 && _aliveBalls == 0)
        {
            if (!_levelCleared)
            {
                await Task.Delay(5);
                int ltm = LevelGenerator.Instance.GetLinesToMove();
                LevelHolderController.Instance.MoveUp(ltm);
                float mltplr = ((ltm - 1f) / 1.5f);
                _ballToSpawn = Mathf.CeilToInt(_restoredBalls * (mltplr >= 1 ? mltplr : 1f));
                _ballsCount.text = $"x {_ballToSpawn}";
                _ballsCountCanvas.gameObject.SetActive(true);
                Time.timeScale = 1.5f;
                LevelMove?.Invoke();
            }
            else
            {
                Time.timeScale = 1.5f;
                FinishLevel();
            }
        }
    }

    public static Vector3 RoundVectors(Vector3 value, int digits = 2)
    {
        return new Vector3(
            (float)Math.Round((double)value.x, digits),
            (float)Math.Round((double)value.y, digits),
            (float)Math.Round((double)value.z, digits));
    }

    public static float RoundFloat(float value, int digits = 2)
    {
        return (float)Math.Round((double)value, digits);
    }
}
