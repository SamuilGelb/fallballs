﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuStart : MonoBehaviour
{
    public Button PlayButton;

    void Start()
    {
        // Only specifying the sceneName or sceneBuildIndex will load the Scene with the Single mode
        int level = Container.Get<UserDataController>().data?.AccountLevel ?? 1;
        PlayButton.GetComponentInChildren<TextMeshProUGUI>().text = $"Play {level}";

        PlayButton.onClick.AddListener(StartGame);
    }

    private void StartGame()
    {
        Container.Get<SoundController>().Play(SoundType.ButtonClick);
        SceneManager.LoadSceneAsync("GameScene");
    }
}
