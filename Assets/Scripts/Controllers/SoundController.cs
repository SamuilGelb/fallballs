using UnityEngine;

public class SoundController : BaseController
{
    [SerializeField] private AudioSource _soundSource;
    [SerializeField] private AudioSource _musicSource;

    private bool isSoundPlaying => _soundSource.isPlaying;
    private bool isMusicPlaying => _musicSource.isPlaying;

    public override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject.transform.parent);
    }

    protected override void AddToContainer()
    {
        Container.Add<SoundController>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<SoundController>();
    }

    private void Mute(AudioType audioType, bool value)
    {
        switch (audioType)
        {
            case AudioType.Sound:
                _soundSource.mute = value;
                break;
            case AudioType.Music:
                _musicSource.mute = value;
                break;
            default:
                _musicSource.mute = value;
                _soundSource.mute = value;
                break;
        }
    }

    private void Pause(AudioType audioType, bool value)
    {
        switch (audioType)
        {
            case AudioType.Sound:
                if (value)
                {
                    _soundSource.Pause();
                }
                else
                {
                    _soundSource.UnPause();
                }
                break;
            case AudioType.Music:
                if (value)
                {
                    _musicSource.Pause();
                }
                else
                {
                    _musicSource.UnPause();
                }
                break;
            default:
                _soundSource.Pause();
                _musicSource.Pause();
                break;
        }
    }

    public void Stop(AudioType audioType)
    {
        switch (audioType)
        {
            case AudioType.Sound:
                _soundSource.Stop();
                break;
            case AudioType.Music:
                _musicSource.Stop();
                break;
            default:
                _soundSource.Stop();
                _musicSource.Stop();
                break;
        }
    }

    public void Play(MusicType musicType)
    {
        if (isMusicPlaying)
        {
            Stop(AudioType.Music);
        }

        AudioClip clipToPlay = Resources.Load<AudioClip>($"Audio/Music/{musicType.ToString("f")}");

        _musicSource.clip = clipToPlay;
        _musicSource.Play();
    }

    public void Play(SoundType soundType)
    {
        if (isSoundPlaying)
        {
            Stop(AudioType.Sound);
        }

        AudioClip clipToPlay = Resources.Load<AudioClip>($"Audio/Sound/{soundType.ToString("f")}");

        _soundSource.PlayOneShot(clipToPlay);
    }

}

public enum AudioType
{
    Sound,
    Music
}

public enum SoundType
{
    None,
    ButtonClick,
    BallCollide,
    PopUpShow,
    PopUpHide,
    Wictory,
    Lose
}

public enum MusicType
{
    MainTheme,
    GameSound,
    Wictory,
    Lose
}