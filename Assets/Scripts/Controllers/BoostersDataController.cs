using System.Collections.Generic;
using UnityEngine;

public class BoostersDataController : BaseController
{
    [SerializeField] private List<BoosterData> _boostersData = new List<BoosterData>();

    public override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }

    public BoosterData GetData(BoosterType type)
    {
        BoosterData founded = _boostersData.Find(x => x.Type == type);
        return founded;
    }

    protected override void AddToContainer()
    {
        Container.Add<BoostersDataController>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<BoostersDataController>();
    }
}
