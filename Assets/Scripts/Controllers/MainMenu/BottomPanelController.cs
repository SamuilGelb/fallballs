using UnityEngine;
using UnityEngine.UI;

public class BottomPanelController : BaseController
{
    [SerializeField] private Button _shopButton;
    [SerializeField] private Button _lobbyButton;
    [SerializeField] private Button _tournamentButton;
    [SerializeField] private GameObject _glowShop;
    [SerializeField] private GameObject _glowLobby;
    [SerializeField] private GameObject _glowTournaments;
    private GameObject _currentGlow;

    public override void Awake()
    { 
        base.Awake();
        _shopButton.onClick.AddListener(() => OnMainMenuItemClick(MainMenuItem.Shop));
        _lobbyButton.onClick.AddListener(() => OnMainMenuItemClick(MainMenuItem.Lobby));
        _tournamentButton.onClick.AddListener(() => OnMainMenuItemClick(MainMenuItem.Tournament));
    }

    public void Start()
    {
        Container.Get<MainMenuController>().OnMenuItemChanged += BottomPanelController_OnMenuItemChanged;
    }

    private void BottomPanelController_OnMenuItemChanged(MainMenuItem item)
    {
        ChangeGlowOnButton(item);
    }

    private void OnMainMenuItemClick(MainMenuItem Item)
    {
        Container.Get<MainMenuController>().ChangeMenuItem(Item);
        ChangeGlowOnButton(Item);
    }
    private void ChangeGlowOnButton(MainMenuItem some)
    {
        if (_currentGlow == null)
        {
            _currentGlow = _glowLobby;
        }
        _currentGlow.gameObject.SetActive(false);
        if (some == MainMenuItem.Shop)
        {
            _currentGlow = _glowShop;
        }
        if (some == MainMenuItem.Lobby)
        {
            _currentGlow = _glowLobby;
        }
        if (some == MainMenuItem.Tournament)
        {
            _currentGlow = _glowTournaments;
        }
        _currentGlow.gameObject.SetActive(true);
        
    }    

    protected override void AddToContainer()
    {
        Container.Add<BottomPanelController>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<BottomPanelController>();
    }
}
