using UnityEngine;
using UnityEngine.UI;

using TMPro;

namespace FallBalls.MainMenu
{
    public class TopPanelCotroller : BaseController
    {


        [SerializeField] private Button _DonatButton;

        [SerializeField] private Button _SettingButton;

        public Image Image;
        public TextMeshProUGUI Name;
        public TextMeshProUGUI Money;

        public override void Awake()
        {
            base.Awake();
            int AvatarId = Container.Get<UserDataController>().data.Avatar;
            Image.sprite = Resources.Load<Sprite>($"Avatars/{AvatarId}");
            string NameId = Container.Get<UserDataController>().data.NickName;
            Name.text = NameId;

            int MoneyId = (int)Container.Get<UserDataController>().data.GameMoney;
            Money.text = $"{MoneyId}";

            _DonatButton.onClick.AddListener(DonatButton);

            _SettingButton.onClick.AddListener(SettingButton);
        }

        public override void Init()
        {



        }

        void DonatButton()
        {
            Debug.Log("You have clicked the Donat Button!");
        }

        void SettingButton()
        {
            Debug.Log("You have clicked the Setting Button!");
        }

        protected override void AddToContainer()
        {
            Container.Add<TopPanelCotroller>(this);
        }

        protected override void RemoveFromContainer()
        {
            Container.Remove<TopPanelCotroller>();
        }
    }
}