using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BoosterViewController : MonoBehaviour
{
    [SerializeField] private Image _boosterImage;
    [SerializeField] private TextMeshProUGUI _boosterCount;
    [SerializeField] private Button _boosterBuyButton;

    private BoosterData _data;

    public void Awake()
    {
        _boosterBuyButton.onClick.AddListener(OnBuyBoosterClickHandler);
    }

    private void OnBuyBoosterClickHandler()
    {
        if (_data != null)
            Container.Get<MainMenuController>().ChangeMenuItem(MainMenuItem.Shop);
    }

    public void Init(BoosterData data, int currentCount)
    {
        if (data != null)
            _data = data;
        _boosterImage.sprite = _data.BoosterSprite;
        _boosterCount.text = $"{currentCount}";
    }
}
