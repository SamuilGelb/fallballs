using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LobbyController : BaseController
{
    [SerializeField] private BoosterViewController _ballsMultiplerViewController;
    [SerializeField] private BoosterViewController _fieldDamageViewController;
    [SerializeField] private BoosterViewController _itemSpawnerViewController;

    [SerializeField] private LevelItemViewController _levelitemPrefab;
    [SerializeField] private RectTransform _levelItemSpawnPosition;

    [SerializeField] private Button _startGame;

    private List<LevelItemViewController> _spawnedItems = new List<LevelItemViewController>();

    public override void Awake()
    {
        base.Awake();
        BoostersDataController boostersDataController = Container.Get<BoostersDataController>();
        UserDataController userData = Container.Get<UserDataController>();
        BoosterData boosterData = boostersDataController.GetData(BoosterType.BallsX2);
        _ballsMultiplerViewController.Init(boosterData, userData.data.Boosters[(int)BoosterType.BallsX2]);
        boosterData = boostersDataController.GetData(BoosterType.DamageOnMap);
        _fieldDamageViewController.Init(boosterData, userData.data.Boosters[(int)BoosterType.DamageOnMap]);
        boosterData = boostersDataController.GetData(BoosterType.SpawnItems);
        _itemSpawnerViewController.Init(boosterData, userData.data.Boosters[(int)BoosterType.SpawnItems]);

        var file = Resources.Load<TextAsset>("Levels/LevelList").text;
        LevelListData list = JsonConvert.DeserializeObject<LevelListData>(file);

        int countToSpawn = list.Count >= 50 ? list.Count : 50;

        for (int i = 0; i < countToSpawn; i++)
        {
            var item = Instantiate(_levelitemPrefab, _levelItemSpawnPosition);
            if (userData.data.PassedLevels?.Any(x => x?.LevelNum == i + 1)??false)
            {
                item.Init(userData.data.PassedLevels.Find(x => x.LevelNum == i + 1));
            }
            else
            {
                if (i <= userData.data.AccountLevel + 3)
                {
                    item.Init(i + 1, false || i + 1 > list.Count);
                }
                else
                {
                    item.Init(i + 1, true);
                }
            }
            _spawnedItems.Add(item);
        }
        StartCoroutine(LoadMoreElements(50));
        _startGame.onClick.AddListener(OnStartGameClickHandler);
    }

    private IEnumerator LoadMoreElements(int count)
    {
        UserDataController userData = Container.Get<UserDataController>();
        var file = Resources.Load<TextAsset>("Levels/LevelList").text;
        LevelListData list = JsonConvert.DeserializeObject<LevelListData>(file);
        int startPoint = _spawnedItems.Count;
        int endPoint = startPoint + count;
        for (int i = startPoint; i < endPoint; i++)
        {
            yield return new WaitForFixedUpdate();
            var item = Instantiate(_levelitemPrefab, _levelItemSpawnPosition);
            if (userData.data.PassedLevels.Any(x => x.LevelNum == i + 1))
            {
                item.Init(userData.data.PassedLevels.Find(x => x.LevelNum == i + 1));
            }
            else
            {
                if (i <= userData.data.AccountLevel + 3)
                {
                    item.Init(i + 1, false || i + 1 > list.Count);
                }
                else
                {
                    item.Init(i + 1, true);
                }
            }
            _spawnedItems.Add(item);
        }
    }

    private void OnStartGameClickHandler()
    {
        Container.Get<UserDataController>()._levelToStart = Container.Get<UserDataController>().data.AccountLevel;
        SceneManager.LoadSceneAsync("GameScene");
    }

    protected override void AddToContainer()
    {
        Container.Add<LobbyController>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<LobbyController>();
    }
}
