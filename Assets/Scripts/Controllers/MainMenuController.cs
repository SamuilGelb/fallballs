using System;
using UnityEngine;

public class MainMenuController : BaseController
{
    [SerializeField] private RectTransform _safeArea;
    [SerializeField] private MainMenuItem _currentItem;
    
    public bool AnimationOnGoing { get; private set; }
    public float ScreenWidth { get; private set; }
    public float CurrentPosition => _safeArea.anchoredPosition.x;

    private const MainMenuItem DEFAULT_ITEM = MainMenuItem.Lobby;

    public delegate void MenuItemChange(MainMenuItem item);
    public event MenuItemChange OnMenuItemChanged;

    public override void Awake()
    {
        base.Awake();
        ScreenWidth = 1440;
        _safeArea.anchoredPosition = Vector2.zero;
        _currentItem = DEFAULT_ITEM;
    }

    public void ChangeMenuItem(MainMenuItem itemToChange)
    {
        _currentItem = itemToChange;
        OnMenuItemChanged?.Invoke(itemToChange);
        DoAnimation();

    }

    private void DoAnimation()
    {
        AnimationOnGoing = true;
        LeanTween.value(_safeArea.gameObject, 
                        new Vector2(CurrentPosition, 0), 
                        new Vector2((int)_currentItem * ScreenWidth, 0), 
                        0.3f).
          setOnUpdate((Vector2 value) =>
          {
              _safeArea.anchoredPosition = value;
          }).
          setOnComplete(() => 
          {
              AnimationOnGoing = false;
          });
    }


    protected override void AddToContainer()
    {
        Container.Add<MainMenuController>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<MainMenuController>();
    }
}

[System.Serializable]
public enum MainMenuItem
{
    Shop = 1,
    Lobby = 0,
    Tournament = -1
}