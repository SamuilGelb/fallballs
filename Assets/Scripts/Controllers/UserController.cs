public class UserController : BaseController
{
    public UserDataForm data = new UserDataForm() { AccountLevel = 12 };
    protected override void AddToContainer()
    {
        Container.Add<UserController>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<UserController>();
    }
}