using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace FallBalls.GamePlay
{
    public class TopPanelCotroller : BaseController
    {
        [SerializeField] private TextMeshProUGUI _levelNum;
        [SerializeField] private TextMeshProUGUI _scoreCount;
        [SerializeField] private TextMeshProUGUI _timer;
        [SerializeField] private Slider _scoreSlider;
        [SerializeField] private Image _star1;
        [SerializeField] private Image _star2;
        [SerializeField] private Image _star3;

        public void Start()
        {
            var userData = Container.Get<UserDataController>();
            _levelNum.text = $"Level: {userData._levelToStart}";
            _scoreCount.text = $"Score: {0}";
            _timer.gameObject.SetActive(false);
            GameManager.Instance.OnScoreUpdated += OnScoreUpdated;
            _scoreSlider.minValue = 0;
            _scoreSlider.maxValue = GameManager.Instance.LevelGenerator.MaxScore;
            _scoreSlider.value = 0;
        }

        private void OnScoreUpdated(int score)
        {
            _scoreCount.text = $"Score: {score}";
            _scoreSlider.value = score;
            if (score >= _scoreSlider.maxValue*0.5f)
                _star1.gameObject.SetActive(true);
            if (score >= _scoreSlider.maxValue * 0.75f)
                _star2.gameObject.SetActive(true);
            if (score >= _scoreSlider.maxValue)
                _star3.gameObject.SetActive(true);
        }

        protected override void AddToContainer()
        {
            Container.Add(this);
        }

        protected override void RemoveFromContainer()
        {
            Container.Remove<TopPanelCotroller>();
        }

    }
}