using System;
using UnityEngine;

public class LoadingController : MonoBehaviour
{
    [SerializeField] private RectTransform _panel;

    public delegate void OnChangeState();

    public event OnChangeState OnShow;
    public event OnChangeState OnHide;


    public void Awake()
    {
        Show();
    }

    public void Show()
    {
        _panel.gameObject.SetActive(true);
        OnShow?.Invoke();
    }

    public void Hide()
    {
        _panel.gameObject.SetActive(false);
        OnHide?.Invoke();
    }

}
