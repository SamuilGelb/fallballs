using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class SettingsController : PopupBase
{
    [SerializeField] private Image _avatarImage;
    [SerializeField] private Button _editAvatarButton;
    [SerializeField] private TextMeshProUGUI _userName;
    [SerializeField] private Button _editNameButton;
    [SerializeField] private Toggle _soundToggle;
    [SerializeField] private Toggle _musicToggle;
    [Header("Social")]
    [SerializeField] private Button _facebookButton;
    [SerializeField] private Button _googleButton;
    [SerializeField] private Button _someButton;

    private const string FACEBOOK_URL = "facebook.com";
    private const string GOOGLE_URL = "google.com";
    private const string SOME_URL = "";

    private void Awake()
    {
        _editAvatarButton.onClick.AddListener(OnEditAvatarButtonClick);
        _soundToggle.onValueChanged.AddListener(OnSoundOrMusicToogleValueChanged);
        _musicToggle.onValueChanged.AddListener(OnSoundOrMusicToogleValueChanged);
        _editNameButton.onClick.AddListener(OnEditNameButtonClick);
        _facebookButton.onClick.AddListener(OnFacebookButtonClick);
        _googleButton.onClick.AddListener(OnGoogleButtonClick);
        _someButton.onClick.AddListener(OnSomeButtonClick);
    }

    private void OnEditAvatarButtonClick()
    {
        Debug.Log("Edit avatar");
    }

    private void OnSoundOrMusicToogleValueChanged(bool value)
    {
        //Container.Get<UserController>().UpdateSettings(_soundToggle.isOn, _musicToggle.isOn);
    }

    private void OnSomeButtonClick()
    {
        Debug.Log(SOME_URL);
    }

    private void OnGoogleButtonClick()
    {
        Application.OpenURL(GOOGLE_URL);
    }

    private void OnFacebookButtonClick()
    {
        Application.OpenURL(FACEBOOK_URL);
    }

    private void OnEditNameButtonClick()
    {
        Debug.Log("EditName");
    }

    protected override void AddToContainer()
    {
        PopupContainer.Add<SettingsController>(this);
    }

    protected override void RemoveFromContainer()
    {
        PopupContainer.Remove<SettingsController>();
    }
}
