using UnityEngine;

public abstract class BaseController : MonoBehaviour
{
    public virtual void Awake()
    {
        AddToContainer();
    }

    protected abstract void AddToContainer();

    protected abstract void RemoveFromContainer();

    public virtual void Init()
    {

    }

    public virtual void OnDestroy()
    {
        RemoveFromContainer();
    }
}
