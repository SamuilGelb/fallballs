using UnityEngine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

public class UserDataController : BaseController
{
    public UserDataForm data = new UserDataForm();
    public int _levelToStart;
    public delegate void DataUpdated();
    public static event DataUpdated DataUpdate;
    //[SerializeField] private bool isNewPlayer = true; //for setting regestration date

    public override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject.transform.parent);
        data.AccountLevel = 1;
        if (PlayerPrefs.HasKey(PlayerPrefsConstants.ID) && PlayerPrefs.HasKey(PlayerPrefsConstants.RegestrationDate))
        {
            UpdateData();
        }
        else
        {
            RegisterNewPlayer();
        }
    }

    public void RegisterNewPlayer()
    {
        data.ID = SystemInfo.deviceUniqueIdentifier;
        PlayerPrefs.SetString(PlayerPrefsConstants.ID, data.ID);
        data.Settings = new UserDataForm.Settigns() { IsMusicOn = true, IsSoundOn = true, LikeOnSocial = false };
        data.Avatar = UnityEngine.Random.Range(0, 6);
        data.NickName = "NickName";
        PlayerPrefs.SetInt(PlayerPrefsConstants.Avatar, data.Avatar);
        PlayerPrefs.SetString(PlayerPrefsConstants.NickName, data.NickName);
        PlayerPrefs.SetInt(PlayerPrefsConstants.IsMusicOn, data.Settings.IsMusicOn ? 1 : 0);
        PlayerPrefs.SetInt(PlayerPrefsConstants.IsSoundOn, data.Settings.IsSoundOn ? 1 : 0);
        PlayerPrefs.SetInt(PlayerPrefsConstants.LikeOnSocial, data.Settings.LikeOnSocial ? 1 : 0);
        PlayerPrefs.SetInt(PlayerPrefsConstants.Level, 1);
        data.RegestrationDate = DateTime.Now;
        PlayerPrefs.SetString(PlayerPrefsConstants.RegestrationDate, data.RegestrationDate.ToString());
        PlayerPrefs.SetString(PlayerPrefsConstants.Boosters, JsonConvert.SerializeObject(data.Boosters));
    }

    public void ChangeNameAndAvatar(string newName, int i)
    {
        if (!string.IsNullOrEmpty(newName) && newName.Length >= 3)
        {
            data.NickName = newName;
            SaveData();
        }
        else
        {
            Debug.Log("��� ��� ���? ������ ���! ��� ���������?!?!?!");
        }
        if (i >= 1 && i <= 6)
        {
            data.Avatar = i;
            SaveData();
        }
        else
        {
            Debug.Log("��� ������ �������... ��� �� ����� ���� ���� ��������! ����...");
        }
    }

    public void ChangeAccauntLevel(int level)
    {
        data.AccountLevel = level;
        SaveData();
    }
    public void ChangeMoneyOnAcc(int p)
    {
        data.GameMoney += p;
        SaveData();
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt(PlayerPrefsConstants.Avatar, data.Avatar);
        PlayerPrefs.SetInt(PlayerPrefsConstants.Level, data.AccountLevel);
        PlayerPrefs.SetInt(PlayerPrefsConstants.Money, data.GameMoney);

        PlayerPrefs.SetString(PlayerPrefsConstants.NickName, data.NickName);
        PlayerPrefs.SetString(PlayerPrefsConstants.Boosters, JsonConvert.SerializeObject(data.Boosters));
        PlayerPrefs.SetString(PlayerPrefsConstants.PassedLevels, JsonConvert.SerializeObject(data.PassedLevels));
        PlayerPrefs.SetString(PlayerPrefsConstants.LastOnline, DateTime.Now.ToString());
        PlayerPrefs.SetInt(PlayerPrefsConstants.IsMusicOn, data.Settings.IsMusicOn ? 1 : 0);
        PlayerPrefs.SetInt(PlayerPrefsConstants.IsSoundOn, data.Settings.IsSoundOn ? 1 : 0);
        PlayerPrefs.SetInt(PlayerPrefsConstants.LikeOnSocial, data.Settings.LikeOnSocial ? 1 : 0);

    }

    public void UpdateBooster(BoosterType type, int countToUpdate)
    {
        data.Boosters[(int)type] += countToUpdate;
        SaveData();
    }

    public void PassLevel(PassedLevelData passedLevelData)
    {
        if (data.PassedLevels.Any(x => x.LevelNum == passedLevelData.LevelNum))
        {
            var pld = data.PassedLevels.Find(x => x.LevelNum == passedLevelData.LevelNum);
            if (pld.Score < passedLevelData.Score)
            {
                pld.LastGameDate = passedLevelData.LastGameDate;
                pld.Score = passedLevelData.Score;
                pld.Stars = passedLevelData.Stars;
            }
        }
        else
        {
            data.PassedLevels.Add(passedLevelData);
        }
        data.AccountLevel = Mathf.Max(passedLevelData.LevelNum + 1, data.AccountLevel);
        SaveData();
    }

    public void UpdateData()
    {
        data.ID = PlayerPrefs.GetString("ID");
        data.Avatar = PlayerPrefs.GetInt(PlayerPrefsConstants.Avatar);
        data.AccountLevel = PlayerPrefs.GetInt(PlayerPrefsConstants.Level);
        data.GameMoney = PlayerPrefs.GetInt(PlayerPrefsConstants.Money);
        data.Boosters = JsonConvert.DeserializeObject<List<int>>(PlayerPrefs.GetString(PlayerPrefsConstants.Boosters));
        string passedLevels = PlayerPrefs.GetString(PlayerPrefsConstants.PassedLevels);
        data._passedLevels = JsonConvert.DeserializeObject<List<PassedLevelData>>(passedLevels);
        data.NickName = PlayerPrefs.GetString(PlayerPrefsConstants.NickName);

        if (data.Settings == null)
            data.Settings = new UserDataForm.Settigns() { IsMusicOn = true, IsSoundOn = true };

        if (PlayerPrefs.GetInt(PlayerPrefsConstants.IsMusicOn) == 1)
        {
            data.Settings.IsMusicOn = true;
        }
        else
        {
            data.Settings.IsMusicOn = false;
        }
        if (PlayerPrefs.GetInt(PlayerPrefsConstants.IsSoundOn) == 1)
        {
            data.Settings.IsSoundOn = true;
        }
        else
        {
            data.Settings.IsSoundOn = false;
        }
        if (PlayerPrefs.GetInt(PlayerPrefsConstants.LikeOnSocial) == 1)
        {
            data.Settings.LikeOnSocial = true;
        }
        else
        {
            data.Settings.LikeOnSocial = false;
        }
        DataUpdate?.Invoke();
    }

    public void SetLastOnline()
    {
        data.LastOnline = DateTime.Now;
        PlayerPrefs.SetString(PlayerPrefsConstants.LastOnline, data.LastOnline.ToString());
    }


    protected override void AddToContainer()
    {
        Container.Add<UserDataController>(this);
    }

    protected override void RemoveFromContainer()
    {
        Container.Remove<UserDataController>();
    }
}
