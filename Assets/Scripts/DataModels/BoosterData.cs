using UnityEngine;

[CreateAssetMenu(fileName = "NewBoosterData", menuName = "FallBalls/BoosterData")]
public class BoosterData : ScriptableObject
{
    public Sprite BoosterSprite;
    public int BoosterCost;
    public int CountToBuy;
    public string Name;
    public string Description;
    public BoosterType Type;
}
