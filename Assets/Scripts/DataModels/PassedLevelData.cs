using System;

[Serializable]
public class PassedLevelData : IComparable<PassedLevelData>
{
    public int LevelNum;
    public int Score;
    public int Stars;
    public DateTime LastGameDate;

    public int CompareTo(PassedLevelData other)
    {
        return LevelNum - other.LevelNum;
    }
}
